from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from ideas.views import (
    CategoryListView,
    ImageCreateView,
    IdeaListCreateView,
    IdeaRetrieveView,
    IdeaVotingView,
)


urlpatterns = [
    path('api/admin/', admin.site.urls),
    path('api/categories/', CategoryListView.as_view()),
    path('api/images/', ImageCreateView.as_view()),
    path('api/ideas/', IdeaListCreateView.as_view()),
    path('api/ideas/<int:pk>/', IdeaRetrieveView.as_view()),
    path('api/upvote/<int:pk>/', IdeaVotingView.as_view(), {'vote': 'up'}),
    path('api/downvote/<int:pk>/', IdeaVotingView.as_view(), {'vote': 'down'}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
