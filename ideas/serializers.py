from rest_framework import serializers

from ideas.models import (
    Category,
    Image,
    Idea,
)


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name')


class SuperCategorySerializer(serializers.ModelSerializer):
    subcategories = CategorySerializer(many=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'subcategories')


class ImageSerializer(serializers.ModelSerializer):
    thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        model = Image
        fields = ('id', 'file', 'thumbnail')


class IdeaSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    subcategory = CategorySerializer()
    images = ImageSerializer(many=True)

    class Meta:
        model = Idea
        fields = ('id', 'category', 'subcategory', 'status', 'title', 'description', 'author',
                  'address', 'upvotes', 'downvotes', 'images', 'created_at', 'updated_at')


class IdeaCreateSerializer(serializers.ModelSerializer):
    images = serializers.PrimaryKeyRelatedField(required=False, many=True,
                                                queryset=Image.objects.filter(idea__isnull=True))

    class Meta:
        model = Idea
        fields = ('id', 'category', 'subcategory', 'status', 'title', 'description', 'author', 'email',
                  'address', 'upvotes', 'downvotes', 'images', 'created_at', 'updated_at')
