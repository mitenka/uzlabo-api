# Generated by Django 2.2.6 on 2019-10-23 18:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ideas', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='idea',
            name='latitude',
        ),
        migrations.RemoveField(
            model_name='idea',
            name='longitude',
        ),
    ]
