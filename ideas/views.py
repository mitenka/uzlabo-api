from rest_framework import (
    generics,
    mixins,
)
from rest_framework.response import Response

from ideas.models import (
    Category,
    Image,
    Idea,
)
from ideas.serializers import (
    SuperCategorySerializer,
    ImageSerializer,
    IdeaSerializer,
    IdeaCreateSerializer,
)


class CategoryListView(generics.ListAPIView):
    pagination_class = None
    serializer_class = SuperCategorySerializer

    def get_queryset(self):
        return Category.objects.filter(parent=None).all()


class ImageCreateView(generics.CreateAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer


class IdeaListCreateView(generics.ListCreateAPIView):
    queryset = Idea.objects.exclude(status='banned')

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return IdeaCreateSerializer
        return IdeaSerializer


class IdeaRetrieveView(generics.RetrieveAPIView):
    queryset = Idea.objects.all()
    serializer_class = IdeaSerializer


class IdeaVotingView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Idea.objects.all()
    serializer_class = IdeaSerializer

    def post(self, request, *args, **kwargs):
        idea = self.get_object()

        if kwargs['vote'] == 'up':
            idea.upvotes += 1
        if kwargs['vote'] == 'down':
            idea.downvotes += 1

        idea.save()
        return Response(self.serializer_class(idea, context={'request': request}).data)
